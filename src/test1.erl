%%%-------------------------------------------------------------------
%%% @author ishiev
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 25. сен 2015 16:24
%%%-------------------------------------------------------------------
-module(test1).
-author("ishiev").

%% API
-compile(export_all).

%% Часть 4

index(I) when is_integer(I) -> I + 1;
index(I) when is_float(I) -> I + 2;


index(I) when is_list(I) -> [index(A) || A <- I];
index(I) when is_function(I, 0) -> index(I()).


f1() -> fun f2/0.
-spec f2() -> fun().
f2() -> fun f1/0.

fac(N) when N == 0 -> 1;
fac(N) when N > 0 -> N * fac(N-1).

fact(N) -> fact(N, 1).

fact(0, Acc) -> Acc;
fact(N, Acc) when N > 0 -> fact(N-1, N*Acc).


len([]) -> 0;
len([_|T]) -> 1 + len(T).

lent(L) -> lent(L, 0).
lent([], Acc) -> Acc;
lent([_|T], Acc) -> lent(T, Acc+1).


qs([]) -> [];
qs([Pivot|Rest]) ->
  {Smaller, Larger} = part(Pivot, Rest, [], []),
  qs(Smaller) ++ [Pivot] ++ qs(Larger);
qs(X) -> X.

part(_, [], Smaller, Larger) -> {Smaller, Larger};
part(Pivot, [H|T], Smaller, Larger) when H =< Pivot -> part(Pivot, T, [qs(H) | Smaller], Larger);
part(Pivot, [H|T], Smaller, Larger) when H > Pivot -> part(Pivot, T, Smaller, [qs(H) | Larger]).

%% Часть 5

one() -> 1.
two() -> 2.

add(X, Y) -> X() + Y().


inc([]) -> [];
inc([H|T]) -> [H+1|inc(T)].

map(_, []) -> [];
map(F, [H|T]) -> [F(H) | map(F, T)].



filter(P, L) -> lists:reverse(filter(P, L, [])).

filter(_, [], Acc) -> Acc;
filter(P, [H | T], Acc) ->
  A = case P(H) of
    true -> filter(P, T, [H | Acc]);
    false -> filter(P, T, Acc)
  end,
  A.


fold(_, Start, []) -> Start;
fold(F, Start, [H | T]) -> fold(F, F(H, Start), T).


map2(F, L) -> lists:reverse(fold(fun(X, Acc) -> [F(X) | Acc] end, [], L)).


throws(F) ->
  try
    F()
  catch
    error:Error -> {error, caugth, Error,  erlang:get_stacktrace()};
    throw:Throw -> {throw, caugth, Throw,  erlang:get_stacktrace()};
    exit:Exit -> {exit, caugth, Exit,  erlang:get_stacktrace()}
  end.

