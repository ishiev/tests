%%%-------------------------------------------------------------------
%%% @author ishiev
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 17. июн 2015 14:17
%%%-------------------------------------------------------------------
-module(mnesia_tests).
-author("ishiev").

%% API
-compile([export_all]).

-define(TAB_PARTS, 64).

-record(ftable,  {filekey, content, attr = eof}).
-record(findex,  {file, pstart, chunks, props, time}).

%% API

install(Nodes) ->
  ok = mnesia:create_schema(Nodes),
  rpc:multicall(mnesia:system_info(db_nodes), application, start, [mnesia]),
  %% create index table
  {atomic, ok} = mnesia:create_table(findex, [
    {attributes, record_info(fields, findex)},
    {disc_copies, mnesia:system_info(db_nodes)}]),
  %% create data tables
  {atomic, ok} = create_tables().


start() ->
  rpc:multicall(mnesia:system_info(db_nodes), application, start, [mnesia]),
  mnesia:wait_for_tables([findex | tab_list()], 60000).

stop() ->
  rpc:multicall(mnesia:system_info(db_nodes), application, stop, [mnesia]).


upload(FileName) ->
  {ok, File} = file:open(FileName, [read]),
  F = fun() -> mnesia_write_file(FileName, File) end,
  Res = mnesia:activity(sync_dirty, F),
  file:close(File),
  Res.

download(FileName) ->
  Tmp = FileName ++ ".part",
  {ok, File} = file:open(Tmp, [write]),
  F = fun() -> mnesia_read_file(FileName, File) end,
  Res = mnesia:activity(sync_dirty, F),
  file:close(Tmp),
  case Res of
    ok ->
      ok = file:rename(Tmp, FileName);
    _Else ->
      ok = file:delete(Tmp)
  end,
  Res.

delete(FileName) ->
  F = fun() -> mnesia_delete_file(FileName) end,
  mnesia:activity(sync_dirty, F).

%%
%% Internal functions
%%

tab_name(N) -> list_to_atom(lists:flatten(io_lib:format("ftab_~3..0B", [N]))).

tab_list() ->
  L = lists:seq(0, ?TAB_PARTS - 1),
  lists:map(fun(N) -> tab_name(N) end, L).

create_tables() ->
  lists:foldl(fun(Name, Res) ->
    Res = mnesia:create_table(Name, [
      {record_name, ftable},
      {attributes, record_info(fields, ftable)},
      {disc_only_copies, mnesia:system_info(db_nodes)}]) end,
    {atomic, ok},
    tab_list()).

%%
%%  Write functions
%%

mnesia_write_file(FileName, File) ->
  file:position(File, bof),
  {N0, C0} = case mnesia:read(findex, FileName) of
    [#findex{pstart = N, chunks = C}] -> {N, C};
    [] ->
      %% select random start point
      {_, _, Ms} = erlang:timestamp(),
      N = Ms * ?TAB_PARTS div 1000000,
      %% write start index record
      mnesia:write(findex, #findex{
        file = FileName,
        pstart = N,
        chunks = 0}, write),
      {N, 0}
  end,
  {N1, C1} = mnesia_write_file(FileName, File, 0, N0),
  %% delete previous data tail (if exists)
  mnesia_delete_file(FileName, C1, C0, N1),
  %% update index record
  mnesia:write(findex, #findex{
    file = FileName,
    pstart = N0,
    chunks = C1,
    time = os:timestamp()}, write).

mnesia_write_file(FileName, File, ChunkNum, ?TAB_PARTS) ->
  mnesia_write_file(FileName, File, ChunkNum, 0);

mnesia_write_file(FileName, File, ChunkNum, N) ->
  case file:read(File, 512*1024) of
    {ok, Bin} ->
      mnesia:write(tab_name(N), #ftable{
        filekey = {FileName, ChunkNum},
        content = Bin,
        attr = data}, write),
      mnesia_write_file(FileName, File, ChunkNum + 1, N + 1);
    eof ->
      mnesia:write(tab_name(N), #ftable{
        filekey = {FileName, ChunkNum},
        content = <<>>,
        attr = eof}, write),
      {N + 1, ChunkNum + 1}
  end.

%%
%% Read functions
%%

mnesia_read_file(FileName, File) ->
  file:position(File, bof),
  case mnesia:read(findex, FileName) of
    [#findex{chunks = 0}] -> incomplete;  % write operation was aborted
    [#findex{pstart = N}] ->
      mnesia_read_file(FileName, File, 0, N);
    [] -> not_found
  end.

mnesia_read_file(FileName, File, ChunkNum, ?TAB_PARTS) ->
  mnesia_read_file(FileName, File, ChunkNum, 0);

mnesia_read_file(FileName, File, ChunkNum, N) ->
  case mnesia:read(tab_name(N), {FileName, ChunkNum}) of
    [#ftable{content = Bin, attr = data}] ->
      file:write(File, Bin),
      mnesia_read_file(FileName, File, ChunkNum + 1, N + 1);
    [#ftable{attr = eof}] -> ok;
    [] -> incomplete                      % data corruption after write
  end.

%%
%% Deletion functions
%%

mnesia_delete_file(FileName) ->
  case mnesia:read(findex, FileName) of
    [#findex{pstart = N, chunks = Chunks}] ->
      mnesia_delete_file(FileName, 0, Chunks, N),
      mnesia:delete(findex, FileName, write);
    [] -> not_found
  end.

mnesia_delete_file(FileName, ChunkNum, Chunks, ?TAB_PARTS) ->
  mnesia_delete_file(FileName, ChunkNum, Chunks, 0);

%% slow deletion for incomplete data (chunks was not update in index)
mnesia_delete_file(FileName, ChunkNum, 0, N) ->
  case mnesia:read(tab_name(N), {FileName, ChunkNum}) of
    [#ftable{}] ->
      mnesia:delete(tab_name(N), {FileName, ChunkNum}, write),
      mnesia_delete_file(FileName, ChunkNum + 1, 0, N + 1);
    [] -> ok
  end;

%% fast regular deletion
mnesia_delete_file(FileName, ChunkNum, Chunks, N) ->
  case ChunkNum of
    Any when Any >= Chunks -> ok;
    _Else ->
      mnesia:delete(tab_name(N), {FileName, ChunkNum}, write),
      mnesia_delete_file(FileName, ChunkNum + 1, Chunks, N + 1)
  end.





