
-module(test_sup).

-behaviour(supervisor).

%% API
-export([start_link/0, start/0, start_child/2]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor
-define(CHILD(I), {I, {I, start_link, []}, transient, 5000, worker, [I]}).

%% ===================================================================
%% API functions
%% ===================================================================

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

start() ->
    case supervisor:start_link({local, ?MODULE}, ?MODULE, []) of
	{ok, Pid} ->
	    erlang:unlink(Pid),
	    {ok, Pid};
	Else -> Else
    end.

start_child(Sup, Mod) ->
    supervisor:start_child(Sup, ?CHILD(Mod)). 

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init([]) ->
    {ok, { {one_for_one, 5, 10}, []} }.

