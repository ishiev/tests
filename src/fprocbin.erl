% Textfile filtration module
-module(fprocbin).
-export([process/5, fempty/1, fsample/1, fnull/1]).

%% File filtration function ->
%% Read file Name1, рarse strings to list of fields using Delims1 as separation regexp,
%% apply Filter function to each list of fields, join fields back to string with Delims2?
%% write to new file Name2
process(Name1, Name2, Delims1, Delims2, Filter) ->
  Start = os:timestamp(),
  {ok, File1} = file:open(Name1, [binary, read, raw, read_ahead]),
  {ok, File2} = file:open(Name2, [write, raw]),
  ok = filterfile(File1, File2, Delims1, Delims2, Filter),
  ok = file:close(File1),
  ok = file:close(File2),
  io:format("total time taken ~f milliseconds~n", [timer:now_diff(os:timestamp(), Start) / 1000]).

filterfile(File1, File2, Delims1, Delims2, Filter) ->
  case file:read_line(File1) of
    {ok, Data} ->
      case Filter(binary:split(Data, Delims1, [global])) of
        [_ | _] = Res -> ok = file:write(File2, [join(Res, Delims2), 13, 10]);
        [] -> ok
      end,
      filterfile(File1, File2, Delims1, Delims2, Filter);
    eof -> ok
  end.

join([H | T], Delims) ->
  [H, Delims | join(T, Delims)];
join([], _) -> [].

%% Filters -> transforms input list of strings to output one 
%% Callback in filtration function

% Empty filter (no filtration)
fempty(L) -> L.

% Null (returns empty list forever)
fnull(_) -> [].

% Sample trivial filter (supress and reorder some fields, first field repeated) 
fsample(L) ->
  [A, _, C, D | _T] = L,
  [A, C, D, [], [], A].
