%%%-------------------------------------------------------------------
%%% @author ishiev
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 18. июн 2015 12:36
%%%-------------------------------------------------------------------
-module(rpc_file).
-author("ishiev").

%% API
-compile(export_all).


open(Nodes, FileName, Modes) ->
  Res = lists:map(fun(N) -> rpc:block_call(N, file, open, [FileName, Modes], 2000) end, Nodes),
  [ File || {ok, File} <- Res ].

write([H | T], Bytes) ->
  Res = file:write(H, Bytes),
  lists:foldl(fun(F, R) -> R = file:write(F, Bytes) end, Res, T).

read([H | T], Number) ->
  Res = file:read(H, Number),
  lists:foldl(fun(F, R) -> R = file:read(F, Number) end, Res, T).






