% Textfile filtration module
-module(filterproc).
-compile(export_all).

%% File filtration function ->
%% Read file Name1, рarse strings to list of fields using Delims1 as separation regexp,
%% apply Filter function to each list of fields, join fields back to string with Delims2?
%% write to new file Name2
test_short() ->
  process("testdata/test1.csv", "testdata/#test1_out.csv#", [2], [2], fun filterproc:fempty/1).

test_long() ->
  process("testdata/test2.csv", "testdata/#test2_out.csv#", [2], [2], fun filterproc:fempty/1).

process(Name1, Name2, Delims1, Delims2, Filter) ->
  Start = os:timestamp(),
  {ok, File1} = file:open(Name1, [read, raw, read_ahead]),
  {ok, File2} = file:open(Name2, [write, raw]),
  filterfile(File1, File2, Delims1, Delims2, Filter),
  ok = file:close(File1),
  ok = file:close(File2),
  io:format("total time taken ~f milliseconds~n", [timer:now_diff(os:timestamp(), Start) / 1000]).

filterfile(File1, File2, Delims1, Delims2, Filter) ->
  case file:read_line(File1) of
    {ok, Data} ->
      case Filter(tokens(Data, Delims1)) of
        [_ | _] = Res -> ok = file:write(File2, [string:join(Res, Delims2), 13, 10]);
        [] -> ok
      end,
      filterfile(File1, File2, Delims1, Delims2, Filter);
    eof -> ok
  end.

% Custom tokens function (based on string:tokens)
tokens(S, Seps) ->
  tokens1(S, [10 | Seps], []). % add \x10 char as delimiter for correct work

tokens1([C | S], Seps, Toks) ->
  case lists:member(C, Seps) of
    true -> tokens1(S, Seps, [[] | Toks]);
    false -> tokens2(S, Seps, Toks, [C])
  end;
tokens1([], _Seps, Toks) ->
  lists:reverse(Toks).

tokens2([C | S], Seps, Toks, Cs) ->
  case lists:member(C, Seps) of
    true -> tokens1(S, Seps, [lists:reverse(Cs) | Toks]);
    false -> tokens2(S, Seps, Toks, [C | Cs])
  end;
tokens2([], _Seps, Toks, Cs) ->
  lists:reverse([lists:reverse(Cs) | Toks]).

%% Filters -> transforms input list of strings to output one 
%% Callback in filtration function

% Empty filter (no filtration)
fempty(L) -> L.

% Null (returns empty list forever)
fnull(_) -> [].

% Sample trivial filter (supress and reorder some fields, first field repeated) 
fsample(L) ->
  [A, _, C, D | _T] = L,
  [A, C, D, [], [], A].


%% NOT USED! Do not see below this line!
%% List on strings filtration function.
%% Parse strings to tokens separeted by Delims1, apply Filter function to each row, join string back with Delims2
%% Args: List on strings, Delims1, Delims2, Filter function
%% Returns: list of filtered dtrings

filterdata([H | L], Delims1, Delims2, Filter) ->
  case Filter(string:tokens(H, Delims1)) of
    [] -> filterdata(L, Delims1, Delims2, Filter);
    [_ | _] = Res -> [string:join(Res, Delims2) | filterdata(L, Delims1, Delims2, Filter)]
  end;
filterdata([], _, _, _) -> [].    
